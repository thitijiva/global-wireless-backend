const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded())

app.post('/exam', (req, res) => {
  data = {
    msg: req.body.data,
    length: req.body.data.length
  }
  res.send(data)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})